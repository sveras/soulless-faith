package utilities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class AudioPlayer {
    
    private boolean play;
    
    private int tracktoPlay=0;
    private Clip clip;
    private ArrayList<String> paths;
    private File [] files;
    
    public AudioPlayer(ArrayList<String> misPaths){
        
        files=new File[misPaths.size()+1];
        for (int i = 1; i < misPaths.size(); i++) {
            files[i]= new File(misPaths.get(i));
        }
        
    }
    
    public void play(){
        if(clip!=null){
            clip.stop();
        }
    
        try {
                File file=files[tracktoPlay];
                AudioInputStream ais = AudioSystem.getAudioInputStream(file);
                AudioFormat format = ais.getFormat();
                DataLine.Info info= new DataLine.Info(Clip.class,format);
                Clip clp= (Clip) AudioSystem.getLine(info);
                clp.open(ais);
                clip=clp;
                clip.start();
                clip.addLineListener((le) -> {
                    synchronized(this){
                        while(play){
                            
                        play=true;    
                        }
                        
                            notifyAll();
                    }
                });
                play=true;
               

            }catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
                ex.printStackTrace();
                
            }
    }
    
    public void playStop(){
        
        if(!play){
            this.play();
        }else{
            clip.stop();
            play=false;
        }

    }
    
    public void stop(){
        clip.stop();
        play=false;
    }
    
    public void setTrackToPlay(int track){
        tracktoPlay=track;
    }
    
    public int getTrackToPlay(){
        return tracktoPlay;
    }
    
    
    public boolean isPlaying(){
        return play;
    }
   
    
    

    
    
}
