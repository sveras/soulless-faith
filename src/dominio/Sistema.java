
package dominio;

import java.io.Serializable;
import java.util.ArrayList;

public class Sistema implements Serializable{
private ArrayList<String> rutas;
private ArrayList<String> temas;
private ArrayList<String> playList;
private ArrayList<String> rutasPlayList;
private ArrayList<String> rutaImagenes;
private ArrayList<String> imagenes;
private int playing=0;

    
    public Sistema() {
        this.rutas = new ArrayList<>();
        rutas.add("Rutas");
        this.temas = new ArrayList<>();
        temas.add("Temas");
        this.playList=new ArrayList<>();
        playList.add("PlayList");
        rutasPlayList=new ArrayList<>();
        rutasPlayList.add("RutasPlayList");
        rutaImagenes=new ArrayList<>();
        imagenes=new ArrayList<>();
        
    }
    
    public ArrayList<String> getListaRutas() {
        return rutas;
    }

    public ArrayList<String> getRutaImagenes() {
        return rutaImagenes;
    }
    

    public ArrayList<String> getListaTemas() {
        return temas;
    }

    public ArrayList<String> getPlayList() {
        return playList;
    }

    public ArrayList<String> getRutasPlayList() {
        return rutasPlayList;
    }

    public ArrayList<String> getImagenes() {
        return imagenes;
    }
    
    

    public int getPlaying() {
        return playing;
    }
    
    
    public boolean agregarTema(String nombre){
        return this.getListaTemas().add(nombre);
    }
  
    public boolean agregarRuta(String rut){
        return this.getListaRutas().add(rut);
    }
    
    public boolean agregarAPlayList(String tema){
        return this.playList.add(tema);
    }

    public void setPlaying(int playing) {
        this.playing = playing;
    }
    
    
    


    
}
